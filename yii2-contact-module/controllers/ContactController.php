<?php

namespace app\controllers;

use app\components\Helper;
use app\components\HelperE;
use app\models\AnnUsers;
use app\models\Articles;
use app\models\Contact;
use app\models\RegisterForm;
use app\models\SubscribeBox;
use app\models\User;
use app\models\UserPermissions;
use app\models\Users;
use Carbon\Carbon;
use Yii;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Json;
use app\components\PhpList;


use function Symfony\Component\Translation\t;

class ContactController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSso()
    {
        $model = Articles::find()->where("id=1")->one();
        return $this->render('sso', ['model' => $model]);
    }











    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        //$this->layout =  '';
        $model = new Contact();

            if ($this->request->isPost && Yii::$app->request->isAjax) {
                $model->load(Yii::$app->request->post()) ;
                $newId = Contact::find()->max('c_id') + 1;
                $model->c_id= $newId;
                $model->id= $newId;
                $model->verifyCode = $this->request->post('g-recaptcha-response');
                $model->contact(Yii::$app->params['adminEmail']);
                    if ($model->save()) {
                        $result = Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        $result = ['status' => 'success',
                            'title' => Yii::t('cpm', 'success!'),
                            'msg' => Yii::t('cpm', 'your message sent successfully'),
                            'confirm' => Yii::t('cpm', 'ok')];
                        return $result;
                    } else {
                        $result = Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        $result = ['status' => 'error',
                            'title' => Yii::t('cpm', 'error!'),
                            'msg' => Yii::t('cpm', 'some thing went wrong'),
                            'confirm' => Yii::t('cpm', 'ok'),
                            'err'=>$model->getErrors()];

                        return $result;
                    }
                }

        return $this->render('contact', [
            'model' => $model,
        ]);
    }



}
