<?php

namespace muzna\contact;

use Yii;
/**
 * contact module definition class
 */
class contact extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\contact\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {

        parent::init();
        $config = require(__DIR__ . '/config/web.php');
        // custom initialization code goes here
        \Yii::configure($this, $config);
    }
}
