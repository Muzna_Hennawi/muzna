<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model app\models\ContactForm */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\captcha\Captcha;
use yii\helpers\Url;
use yii\web\View;

$this->title = Yii::t('cpm', 'Contact');
//$this->params['breadcrumbs'][] = $this->title;
\app\assets\AppAsset::register($this);
?>
<style>
    div.required label:after {
        content: " * ";
    }
    #v:after {
        content: " * ";
    }

</style>
<script src='https://www.google.com/recaptcha/api.js'></script>

<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container pt-lg-5">
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>


            <p>
                <?= Yii::$app->getSession()->getFlash('contactFormSubmitted') ?>
            </p>

        <?php else: ?>
            <div>
                <?php $form = ActiveForm::begin(['id' => 'contact-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3',
                            'offset' => '',
                            'wrapper' => 'col-sm-9',
                            'error' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>
                <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label(Yii::t('cpm', 'name')) ?>
                <?= $form->field($model, 'email')->label(Yii::t('cpm', 'email')) ?>
                <?= $form->field($model, 'place')->label(Yii::t('cpm', 'country/city')) ?>
                <?= $form->field($model, 'job')->label(Yii::t('cpm', 'Job')) ?>
                <?= $form->field($model, 'company')->label(Yii::t('cpm', 'Company')) ?>
                <?= $form->field($model, 'phone')->label(Yii::t('app', 'Phone')) ?>
                <?= $form->field($model, 'mobile')->label(Yii::t('cpm', 'Mobile')) ?>
                <?= $form->field($model, 'fax')->label(Yii::t('cpm', 'Fax')) ?>
                <?= $form->field($model, 'subject')->input('text',['placeholder' => '..........'])->label(Yii::t('cpm', 'Subject')) ?>
                <?= $form->field($model, 'hear')->dropdownList($model->getHearList(), ['prompt' => '..........','class'=>' form-control pt-0'])->label(Yii::t('cpm', 'Hear')) ?>
                <?= $form->field($model, 'hear_other')->label(Yii::t('cpm', 'other, please specify')) ?>
                <?= $form->field($model, 'content')->textarea(['rows' => 6])->label(Yii::t('cpm', 'Massage Content')) ?>
                <div class="row pb-3 " >
                <div class="col-sm-3"><?= Html::label(Yii::t('cpm', 'Verification Code'),'',['id'=>'v']) ?></div>

                <div class="col-sm-9"><div class="g-recaptcha" id="verify" data-sitekey="6Lfwpj4fAAAAAJqhRRXc4VATiuSEC80GULANFYp8" aria-required="true"></div></div>

                </div>
                <div class="row">
                    <button style="display: none" id="processing-button">
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <?= Yii::t('cpe', 'Processing') ?>
                    </button>
                    <div class="col-sm-3"></div>
                   <div class="col-sm-9"> <?= Html::submitButton(Yii::t('cpm', 'Contact as'), ['class' => 'btn btn-info',
                        'name' => 'contact-button',
                        'id' => 'contact-btn',
                        'data-pjax' => '0',
                        'data-url' => Url::toRoute('contact')]) ?></div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>


        <?php endif; ?>
    </div>
</div>
<?php
$this->registerJsFile(
    '@web/js/jquery.js',
    ['depends' => [], 'position' => View::POS_END]
);
$this->registerJsFile(
    '@web/js/e/bootstrap.min.js',
    ['depends' => [], 'position' => View::POS_END]
);
$this->registerJsFile(
    '@web/js/sweetalert2.all.min.js',
    ['depends' => [], View::POS_END]
);
$this->registerJsFile('@web/js/m/submitForms.js?id=' . rand(0, 99),
    ['depends' => [], View::POS_END]);

?>

